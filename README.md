# ChromeCast
#### Its a small android module for integrating chromecast into your projects.

---
# Getting Started
#### VideoCastManager class is responsible for adding chromecast functionality to your projects.So please follow the instructions for integrating cc into your project.

---
# Info about CC module.

##CC module uses the following dependency

'com.google.android.gms:play-services-cast-framework:9.4.0'

###To use it, simply follow the instructions:

* Instantiate the videocastmanager in on-create of your activity or fragment and call the methods "setupCastListener()" ,"oncreate" from the videocastmanager class.

* Implement the interface named "OnCastConnectionListener" into your activity or fragment.It contains the following methods "onCastApplicationConnected,onCastApplicationDisconnected".so when the cc is connected it will fire the onCastApplicationConnected method, inside that method call "loadRemotemedia()" method from videocastmanger class to start cc playback.

---
# Usage
#### 1. Inside onCreate();

```html
 videoCastManager = VideoCastManager.newInstance(this,this);
 videoCastManager.setupCastListener();
 videoCastManager.onCreate(savedInstanceState);
```

#### 2. Inside on onResume();

```html
 videoCastManager.registerCastStateListener();
 videoCastManager.registerSessionManagerListener();
```

#### 3. Inside on onPause();
```html
videoCastManager.unRegisterCastStateListener();
videoCastManager.unRegisterSessionManagerListener();
```
#### 4. For creating CC icon
```html
  public boolean onCreateOptionsMenu(final Menu menu) {
  videoCastManager.createCastMenuIcon(menu);
  return true; 
}
```
#### 4. For creating MiniController
```html
  
    <fragment
        android:id="@+id/castMiniController"
        android:layout_width="fill_parent"
        android:layout_height="wrap_content"
        android:layout_gravity="bottom"
        android:visibility="gone"
        class="com.google.android.gms.cast.framework.media.widget.MiniControllerFragment"/>
}
```

---

##Future Enhancement.
* Customization of cast icon.
* Customization of Minicontroller and cc ChooserDialog.


##Point of contact
* Rahul R S
